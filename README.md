# vimのメモ

## よく使うコマンド

* [最強のチートシート](http://people.csail.mit.edu/vgod/vim/vim-cheat-sheet-en.png)
* [よく使う Vim のコマンドまとめ](https://qiita.com/hide/items/5bfe5b322872c61a6896)
* [vimで複数行のコメント入れたり消したりする(矩形入力・矩形選択削除)](https://qiita.com/rato303/items/035de4117ec0022a0eb0)
* [プログラマが知っておくべき100のVimコマンド](https://loumo.jp/wp/archive/20080701175525/)

### メモ
* ``zz`` : カーソル位置を維持したまま画面中央へ
* ``gg`` : カーソル位置をページ最上部へ移動
* ``G`` : カーソル位置をページ最下部へ移動
* ``w`` : word単位で右へ
* ``b`` : word単位で左へ
*  ``%``: 括弧にカーソルが当たってる場合、対応する括弧に移動
* ``<C-f>`` : 1ページ進む
* ``<C-b>`` : 1ページ戻る
* ``:marks`` : マーク一覧
* ``m[a-zA-Z]`` : カーソル位置をマーク
* ```[a-zA-Z]`` : 指定のマーク位置へジャンプ
* ``:delmarks!`` : マークを全て削除
* ``<C-j>`` : 改行する
* ``<S-j>`` : 改行を消す
* ``yy`` :行ヤンク

### 独自map

* ``j`` :  折り返し時に表示行単位での移動できるようにする
* ``k`` :  堂上
* ``gj`` :  通常の行単位の移動も残す（逆マップ）
* ``gk`` :  堂上
* ``s<LEFT>`` : 画面分割時の右移動
* ``s<RIGHT>`` : 画面分割時の左移動
* ``s<UP>`` : 画面分割時の上移動
* ``s<DOWN>`` : 画面分割時の下移動
* ``<ESC><ESC>`` : エスケープ連打でハイライト解除
* ``s,`` : バッファの順方向移動
* ``s.`` : バッファの逆方向移動
* ``,h`` : ``^``
* ``,l`` : ``$``
* ``;`` : ``:`` よく使う方をshift不要に
* ``:`` : ``;`` 上と入れ替え
* ``\`` : ``,``  (カンマは&lt;Leader&gt;にしているので退避
* ``n`` : ``nzz`` 検索後にジャンプした際に検索単語を画面中央に持ってくる
* ``N`` : ``Nzz`` （同上）
* ``PP`` : ``"0p`` ヤンクレジスタを埋めないようにする
* ``x`` : ``"_x``ヤンクレジスタを埋めないようにする
* ``vv`` : ``v$h`` vを二回で行末まで選択
* ``<Tab>`` : ``%`` 対応してるペア（括弧）に移動
* ``w!`` : スーパーユーザーとして保存
* ``r`` : Redo


## textobj系

### [bps/vim-textobj-python]( https://github.com/bps/vim-textobj-python)

* ``af``: a function（関数名を含む）
* ``if``: inner function（関数内）
* ``ac``: a class（クラス名を含む）
* ``ic``: inner class（クラス内）
* ``[pf`` / ``]pf`` :  move to next/previous function
* ``[pc`` / ``]pc`` :  move to next/previous class

### [kana/vim-textobj-indent](https://github.com/kana/vim-textobj-indent)

* ``ai`` : 選択行以上のインデントがある行を選択（空行含む）
* ``ii`` : 選択行以上のインデントがある行を選択（空行まで）
* ``aI`` : 選択行と同じインデントがある行を選択（空行含む）
* ``aI`` : 選択行と同じインデントがある行を選択（空行まで）

### [osyo-manga/vim-textobj-multiblock](https://github.com/osyo-manga/vim-textobj-multiblock)

* ``ab`` : カーソルから一番近い囲み記号までを範囲選択
* ``ib`` : カーソルから一番近い囲み記号の中までを範囲選択

### [thinca/vim-textobj-comment](https://github.com/thinca/vim-textobj-comment)
* ``ac`` : コメントを範囲選択
* ``ic`` : コメントの内側を範囲選択

### [vim-textobj-from_regexp](https://github.com/osyo-manga/vim-textobj-from_regexp)
* ``i<C-w>`` : 半角英数字文字のみ選択
* ``i,w`` : 0-9A-Za-z_.()[]のみ選択（python用)


## operator系

### [rhysd/vim-operator-surround](https://github.com/rhysd/vim-operator-surround)


* ``sa`` : 例 - `saiw(`  iwに該当する文字を括弧で囲う
* ``sd`` : 例 - `sdiw(`  iwに該当する文字を囲っている括弧を消す
* ``sr`` : 例 - `sra("`  a(に該当する括弧をダブルクォートに置き換える

textobj-multiblockとの合わせ技

* ``sdd`` :   カーソル位置を囲っている文字を消す
* ``srr`` : 例 - `srr"`  カーソル位置を囲っている文字をダブルクォートに置き換える

textobj-betweenとの合わせ技

* ``sdb`` : 例 - `sdb+`  カーソル位置を囲っている+を消す
* ``srb`` : 例 - `srb+-`  カーソル位置を囲っている+を-に置き換える


## Unite系

### [unite.vim](https://github.com/Shougo/unite.vim) 

* ``[Space]+f`` : カレントディレクトリを表示
* ``[Space]+t`` : タブを表示
* ``[Space]+a`` : バッファと最近開いたファイル一覧を表示
* ``[Space]+d`` : 最近開いたディレクトリを表示
* ``[Space]+b`` : バッファを表示
* ``[Space]+r`` : レジストリを表示
* ``[Space]+h`` : yankの履歴
* ``[Space]+o`` : アウトライン表示
* ``[Space]+[Enter]`` : プロジェクトルートからの全ファイル一覧
* ``[Space]+sg`` : grep検索
* ``[Space]+sr`` : grep検索結果の再呼出

### Unite起動時のコマンド一覧

* ``<ESC><ESC>`` : unite終了
* ``<C-w>`` : パス単位で削除

#### normal モード
* ``a`` : 候補が選択されていればaでファイルメニュー
* ``<Space>`` : 候補のマーク/解除
* ``*`` : 全部の候補を選択
* ``<Tab>`` : カーソル下の候補のファイルメニュー
* ``<CR>`` : 選択されている候補に対してdefault実行
* ``gg`` : バッファの先頭へ
* ``G``: バッファの末尾へ
* ``d`` : 候補が選択されていればdelete実行
* ``t`` : 候補が選択されていればtabopen実行
* ``e`` : 候補が選択されていればnarrow実行
* ``x`` : 候補が選択されていれば候補へのジャンプし、default実行

#### insert モード
* ``<C-g>`` : uniteを閉じる
* ``<Space>`` : 候補のマーク/解除
* ``<Tab>`` : アクションの一覧を表示
* ``<CR>`` : 選択されている候補に対してdefault実行
* ``<C-u>`` : 絞り込みのテキストを全て削除
* ``<C-a>`` : 先頭へ
* ``d`` : 候補が選択されていればdelete実行
* ``t`` : 候補が選択されていればtabopen実行
* ``e`` : 候補が選択されていればnarrow実行
* ``x`` : 候補が選択されていれば候補へのジャンプし、default実行
* ``b`` : 候補が選択されていればbookmark実行
* ``yy`` : 候補が選択されていればyank実行
* ``p`` : 候補が選択されていればpreview実行

### [vimfiler.vim](https://github.com/Shougo/vimfiler.vim) 
* ``<Space>e`` : VimFilerExplorerをプロジェクトルートで開く
* ``a`` : アクションを選択
* ``t`` : フォルダを開く・閉じる
* ``<Enter>`` : フォルダを開く・閉じる
* ``T`` : フォルダを再帰的に開く・閉じる
* ``<BS>`` : 親フォルダに移動する
* ``e`` : ファイルを開く
* ``E`` : 新しいウィンドウでファイルを開く
* ``.`` : 隠しファイル（ドットファイル）表示
* ``I`` : ディレクトリを入力し移動
* ``yy`` : フルパスをヤンク（コピー）する
* ``ge`` : システムのファイラーでフォルダを開く
* ``gf`` : ファイルを検索 (find)
* ``gg`` : ファイルをフィルタリング (grep)
* ``x`` : システムの関連付けでファイルを開く
* ``Cc`` : クリップボードにファイルをコピー
* ``Cm`` : クリップボードにファイルを移動
* ``Cp`` : クリップボードからファイルを貼り付け
* ``N`` : 新規ファイルを作成
* ``c`` : ファイルをコピー
* ``m`` : ファイルを移動
* ``r`` : ファイルをリネーム
* ``d`` : ファイルを削除

## git系

### [tpope/vim-fugitive](https://github.com/tpope/vim-fugitive)

* ``<Space>gst`` : 新しい窓を作ってgit statusを表示
* ``<Space>ga`` : 現在開いているソースをgit add
* ``<Space>gr`` : 現在開いているソースの直前のコミット時のソースを表示
* ``<Space>gmv`` : 現在開いているソースをgit mvする
* ``<Space>grm`` : 現在開いているソースをgit rmする
* ``<Space>gcm`` : git commit -v
* ``<Space>gpf`` : git fetch
* ``<Space>gmm`` : git merge
* ``<Space>gbl`` : 現在のソースをgit blame。vimが色づけしてくれる
* ``<Space>gdiff`` : 現在のソースの変更点をvimdiffで表示

### [tpope/vim-fugitive](https://github.com/tpope/vim-fugitive)
* ``<Space>gvv`` : gitvをブラウザモードで起動
* ``<Space>gva`` : gtk --all相当をブラウザモードで起動
* ``<Space>gvf`` : gitvをファイルモードで起動

#### Gstatus時のコマンド一覧
* ``q`` : Gstatusの終了
* ``-`` : git add/reset 切り替え
* ``cc`` : git commit
* ``ca`` : git commit --amend
(直前のコミットに追加したい or 直前のコミットコメントを変更したい)
* ``cA`` : git commit --amend --reuse-message=HEAD
(直前のコミットに追加したい & コメントは直前のを流用)
* ``cva`` : git commit --amend --verbose
(直前のコミットに追加したい & 差分を見つつコメントを書きたい)
* ``cvc`` : git commit --verbose
(差分を見つつコメントを書きたい)
* ``D`` : git diff
* ``U`` : git checkout (ファイルの状態によって意味が変わるので注意)
* ``p`` : git add --path | git reset --path

#### gitv（ブラウザモード）起動時のコマンド一覧
* ``q`` : gitvの終了
* ``<Space>cc`` : git checkout {選択中のブランチ名}
* ``<Space>ps`` : Git push origin {選択中のブランチ名}
* ``<Space>rb`` : git rebase -i {選択中リビジョンのハッシュ値}
* ``<Space>rv`` : Git revert {選択中リビジョンのハッシュ値}
(そのコミットを打ち消すようなコミットを追加)
* ``<Space>cp`` : Git cherry-pick  {選択中リビジョンのハッシュ値}
(別ブランチの指定したコミットのみを自ブランチにコミット)
* ``<Space>rh`` : Git reset --hard {選択中リビジョンのハッシュ値}
(全てを破棄して指定リビジョンに移行したい場合）
* ``<Space>rl`` : Git reflog
(reset --hardでやらかした時に戻したい用)
* ``O`` : 新しいタブでコミットの情報を開く
* ``S`` : diff --stat の表示

### [idanarye/vim-merginal](https://github.com/idanarye/vim-merginal)

* ``<Space>gr`` : vim-merginalの起動・終了（トグル）

#### vim-merginal 起動中
* ``q`` : merginalの終了
* ``cc`` : カーソル位置のブランチをcheckout
* ``aa`` : 新しいbranchを作成
* ``dd`` : カーソル位置のブランチを削除
* ``mm`` : カーソル位置のブランチをmerge
* ``rb`` : カーソル位置のブランチをrebase
* ``ps`` : カーソル位置のブランチをpush
* ``pf`` : カーソル位置のブランチをfetch
* ``rn`` : カーソル位置のブランチをrename

## 便利プラグイン

### [vim-scripts/TaskList.vim](https://github.com/vim-scripts/TaskList.vim)

* ``,t`` : TODOリストを表示


